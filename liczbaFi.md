---
author: Jakub Kielar
title: Liczba Fi
subtitle: Prezentacja 1
date: 04-11-2020
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Wstęp - Leonardo Fibonacci
**Fibonacci** - włoski matematyk, znany jako Leonardo Fibonacci

Jego ojciec, Guglielmo z rodziny Bonacci, zajmował stanowisko dyplomatyczne w Afryce północnej i Fibonacci tam właśnie się kształcił.
Pierwsze lekcje matematyki pobierał od arabskiego nauczyciela w mieście Boużia (dziś algierska [*Bidżaja*](https://pl.wikipedia.org/wiki/Bid%C5%BCaja)).

W czasie swych podróży po Europie i po krajach Wschodu miał okazję poznać osiągnięcia matematyków arabskich i hinduskich, między innymi [**dziesiętny system liczbowy**](https://pl.wikipedia.org/wiki/Dziesi%C4%99tny_system_liczbowy).

\begin{center} Ciąg Fibonacciego \end{center}

Fibonacci opisał i omówił ciąg liczb naturalnych określony rekurencyjnie w swoim dziele Liber abaci jako rozwiązanie zadania o rozmnażaniu się królików.

## Kalendarium złotego podziału

**Kalendarium**

* Fidiasz (490–430 p.n.e.)
  * Figury Parteonu o złotych proporcjach

* Platon (427-347 p.n.e)
  * Dialog Timajos
    * Wielościany platońskie

* Euklides (ok. 325 - ok. 265 p.n.e.)
  * Elementy

* Fibonacci
  * Liber abaci
    * Ciąg Fibonacciego
    * Liczba Fi


## Zastosowanie ciągu Fibonacciego

### Estetyka

De Divina Proportione, trzytomowe dzieło Luca Pacioliego, zgłębia matematykę złotego podziału. Pacioli zalecał stosowanie witruwiańskiego systemu proporcji.

Ilustrowana rysunkami wielościanów foremnych autorstwa Leonarda da Vinci De Divina Proportione miała duży wpływ na pokolenia artystów i architektów.

### Architektura

Fasada Partenonu, jak również wiele elementów na niej i w innych miejscach są określane przez niektórych jako zawierające się w złotych prostokątach.

### Malarstwo

Szesnastowieczny filozof Heinrich Agrippa narysował człowieka na pentagramie wpisanym w koło, co sugeruje związek ze złotym podziałem.

## Zastosowanie ciągu Fibonacciego cd.

\begin{block}{Wymiary książek}

Był czas, gdy odstępstwa od naprawdę pięknych proporcji strony i złotego podziału były rzadkie. Wiele książek wydanych między 1550 i 1770 stosują te proporcje z dokładnością do pół milimetra.

\end{block}

\begin{block}{Przyroda}

Adolf Zeising odkrył złoty podział w ułożeniu gałęzi na pniu roślin i w nerwach liści. Rozszerzył swoje badania na szkielety zwierząt i rozgałęzienia ich żył i nerwów, proporcje składników chemicznych. W zjawiskach tych uznał złoty podział za uniwersalne prawo.

\end{block}

\begin{alertblock}{Muzyka}
Według opinii Leona Harkleroada, „Niektóre z najbardziej chybionych prób powiązania muzyki i matematyki wykorzystywały ciąg Fibonacciego i powiązany z nim złoty podział”
\end{alertblock}

## Matematyka

\begin{block}{Ujemny pierwiastek równania kwadratowego liczby Fi}
$$
\frac{1}{\varphi}=1-\varphi=\frac{1-\sqrt{5}}{2}=-0,61803 39887...
$$
\end{block}

\begin{exampleblock}{Algebraiczny dowód poprawności konstrukcji}
Na mocy twierdzenia Pitagorasa:
$$
c^2=a^2+(\frac{a}{2})^2=\frac{5}{4}a^2
$$
Stosunek długości $\frac{a}{b}$ wynosi:
$$
\frac{a}{b}=\frac{2}{\sqrt{5}-1}=\frac{\sqrt{5}+1}{2}=\varphi
$$
Konstrukcja prowadzi więc do złotego podziału.
\end{exampleblock}

## Matematyka - zdjęcia poglądowe

![](pics/trojkat1.png){height=60% width=60%}

![](pics/trojkat2.png){height=50% width=50%}

## Informatyka - kod Java
Kod na wyznaczenie 10 kolejnych liczb Fibonacciego:

\lstinputlisting[language=java, linerange={3-16}]{src/Fibonacci.java}



## Wartość liczbowa liczby Fi

|System           |Wartość                         |
|:----------------|:------------------------------:|
|Dwójkowo| 1,1001111000110111011…|
|Dziesiętnie| 1,6180339887498948482…|
|Szesnastkowo|1,9E3779B97F4A7C15F39…|
|Ułamek zwykły|$\frac{1+\sqrt{5}}{2}$|
|Szereg nieskończony|$\frac{13}{8}+\Sigma^{\infty}_{n=0}\frac{(-1)^{(n+1)}(2n+1)!}{(n+2)!n!4^{(2n+3)}}$|



## Konstrukcje

:::{.columns}
::::{.column width=0.3}
**\textcolor{red}{Konstrukcja Odoma}**

Kostrukcja $\varphi$  wykorzystującą trójkąt równoboczny: Wpisując trójkąt równoboczny w okrąg, przy odcinku łączącym środki dwóch boków przedłużonym do przecięcia z okręgiem, to te trzy punkty są do siebie w złotej proporcji.

::::
::::{.column width=0.3}
**\textcolor{green}{Pentagram}**

Złoty podział gra istotną rolę w geometrii pentagramu. Wszystkie brzegowe odcinki przecinają się ze sobą w złotym stosunku. Stosunek długości krótszego odcinka do odcinka ograniczonego przez dwie przecinające się krawędziewynosi $\varphi$.

::::
::::{.column width=0.3}
**\textcolor{blue}{Twierdzenie Ptolemeusza}**

Jeżeli dłuższy bok czworoboku i przekątne oznaczymy jako b, a krótsze boki jako a, to twierdzenie Ptolemeusza daje $b^{2}=a^{2}+ab$ z czego wynika
$$
\frac{b}{a}=\frac{1+\sqrt{5}}{2}
$$
::::
:::

